function pcp = pitchClassProfile(Y, fs, fref)
%
% Pitch Class Profiler
%
% Y     --  signal to be analysied, can be a wav file read by calling wavread
% fs    --  sampling rate normally 44100
% fref  --  reference frequence. C3 reference frequency is 130.80Hz. C4 is 261.63Hz. C4 is 'Middle C'
%
% This implementation is based on http://www.music.mcgill.ca/~jason/mumt621/papers5/fujishima_1999.pdf
% Author: Hai Wang
%


fprintf('\n Running PCP ... \n');

N = size(Y, 1);
if (rem(N, 2) == 1),
    Y(N) = [];
    N = size(Y,1);
end;

F = fft(Y);
F = fftshift(F);

%% ===== Display Signal ======
% t = linspace(0,2*pi, N);                                % axis range [0, 2*pi]
% dt = t(2);
% n=[-N/2:1:N/2];
% n(N+1) =[];                                             % remove extral
% freq = 2*pi*n./(N*dt);

% figure;
% plot(freq, abs(F), 'o-');
%

pcp = (0:11)';
Fposi = F( (N/2+1) : N);

M = [0: N/2 - 1];
M = mod(round(12 * log2(fs * M / N / fref)), 12);
M(1) = -1;

pcp = (M == pcp) * (abs(Fposi) .^ 2);


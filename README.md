# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repo contains a 2 layer Neural Network that can train a Guitar Chords detector.

main.m
This has the implementation for Neural Network. Algorithm is based on following paper:
http://jim.afim-asso.org/jim12/pdf/jim2012_08_p_osmalskyj.pdf

pitchClassProfile.m
Pitch Class Profile implementation. Based on following paper: 
http://www.music.mcgill.ca/~jason/mumt621/papers5/fujishima_1999.pdf


Implemented in Octave. 

Version 1.0.0


### How do I get set up? ###

Install Octave and main.m is the entry.
* Guitar Chords records can be downloaded from http://www.montefiore.ulg.ac.be/services/acous/STSI/file/jim2012Chords.zip credit goes to josmalsky@ulg.ac.be.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Owner: wanghai369@gmail.com
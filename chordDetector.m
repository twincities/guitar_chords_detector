%
% Chord recognizer
%

chords = {'a', 'am', 'bm', 'c', 'd', 'dm', 'e', 'em', 'f', 'g'};

X = [];
y = [];
for chordIndex = 1:length(chords),
    chord = chords{1, chordIndex};
    disp(chord);
    for i = 101:200,
        path = strcat('../chords/Guitar_Only/', chord , '/', chord, num2str(i), '.wav');

        fprintf(path, "reading: %s\n");
        signal = wavread(path);      % signal to process

        pcp = pitchClassProfile(signal, 44100, 130.81);

        % normalize
        pcp = pcp / sum(pcp);
        X = [X; pcp'];
        y = [y; chordIndex];

        % figure;
        % plot(pcp);
    end;
    disp(chordIndex);
end;

% save training data
%save training_data_1_100.mat X y;
save training_data_101_200.mat X y;